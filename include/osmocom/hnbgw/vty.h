#pragma once

#include <osmocom/vty/vty.h>

enum osmo_iuh_vty_node {
	HNBGW_NODE = _LAST_OSMOVTY_NODE + 1,
	HNB_NODE,
	IUH_NODE,
	IUCS_NODE,
	IUPS_NODE,
	MGCP_NODE,
	MGW_NODE,
	PFCP_NODE,
	MSC_NODE,
	SGSN_NODE,
};

